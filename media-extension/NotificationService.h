//
//  NotificationService.h
//  media-extension
//
//  Created by Alex Bofu on 6/20/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension<UNUserNotificationCenterDelegate>

@end
