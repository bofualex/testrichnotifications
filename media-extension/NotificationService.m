//
//  NotificationService.m
//  media-extension
//
//  Created by Alex Bofu on 6/20/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

#import "NotificationService.h"

@interface NotificationService ()

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    NSString *receivedIdentifier = [[request.content.userInfo valueForKey:@"aps"] valueForKey:@"malaca"];
    
    [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
        for (int i = 0; i < notifications.count; i++) {
            if ([[[notifications[i].request.content.userInfo valueForKey:@"aps"] valueForKey:@"malaca"] isEqualToString:receivedIdentifier]) {
                [[UNUserNotificationCenter currentNotificationCenter] removeDeliveredNotificationsWithIdentifiers:@[notifications[i].request.identifier]];
                [[UNUserNotificationCenter currentNotificationCenter] removePendingNotificationRequestsWithIdentifiers:@[notifications[i].request.identifier]];
            }
        }
    }];
    
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];
    NSDictionary *userInfo = request.content.userInfo;
    NSLog(@"ceva");
    if (userInfo == nil) {
        [self contentComplete];
        return;
    }
    
    /* Start of User Action Buttons */
    NSString *categoryName = userInfo[@"aps"][@"category"];
    NSString *Button1Title = userInfo[@"action 1"][@"btnTitle"];
    NSString *Button2Title = userInfo[@"action 2"][@"btnTitle"];
    NSString *Button3Title = userInfo[@"action 3"][@"btnTitle"];
    NSString *Button3Id = userInfo[@"action 3"][@"Identifier"];
    NSString *Button1Id = userInfo[@"action 1"][@"Identifier"];
    NSString *Button2Id = userInfo[@"action 2"][@"Identifier"];
    
    UNNotificationAction* dismissAction = [UNNotificationAction
                                           actionWithIdentifier:Button1Id
                                           title:Button1Title
                                           options:UNNotificationActionOptionDestructive];
    
    UNTextInputNotificationAction *replyAction = [UNTextInputNotificationAction
                                                  actionWithIdentifier:Button2Id
                                                  title:Button2Title
                                                  options:UNNotificationActionOptionNone
                                                  textInputButtonTitle:@"trimite"
                                                  textInputPlaceholder:@"tasteazā rāspunsul"];
    
    UNNotificationAction* openBucovinaAction = [UNNotificationAction
                                                actionWithIdentifier:Button3Id
                                                title:Button3Title
                                                options:UNNotificationActionOptionForeground];
    
    UNNotificationCategory* generalCategory = [UNNotificationCategory
                                               categoryWithIdentifier:categoryName
                                               actions:@[dismissAction, replyAction, openBucovinaAction]
                                               intentIdentifiers:@[]
                                               options:UNNotificationCategoryOptionNone];
    
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center setNotificationCategories:[NSSet setWithObjects:generalCategory, nil]];
    center.delegate = self;
    /* End of User Action Buttons */
    
    NSString *mediaUrl = userInfo[@"mediaUrl"];
    NSString *mediaType = userInfo[@"mediaType"];
    
    if (mediaUrl == nil || mediaType == nil) {
        [self contentComplete];
        return;
    }
    
    [self loadAttachmentForUrlString:mediaUrl
                            withType:mediaType
                   completionHandler:^(UNNotificationAttachment *attachment) {
                       if (attachment) {
                           self.bestAttemptContent.attachments = [NSArray arrayWithObject:attachment];
                       }
                       
                       [self contentComplete];
                   }];
}


- (void)serviceExtensionTimeWillExpire {
    [self contentComplete];
}

- (void)contentComplete {
    self.contentHandler(self.bestAttemptContent);
}

- (NSString *)fileExtensionForMediaType:(NSString *)type {
    NSString *ext = type;
    
    if ([type isEqualToString:@"image"]) {
        ext = @"jpg";
    }
    
    if ([type isEqualToString:@"video"]) {
        ext = @"mp4";
    }
    
    if ([type isEqualToString:@"audio"]) {
        ext = @"mp3";
    }
    
    return [@"." stringByAppendingString:ext];
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    NSLog(@"%@", response.notification.request.content.categoryIdentifier);
    completionHandler();
    
    if ([response.notification.request.content.categoryIdentifier isEqualToString:@"bucovina"]) {
        if ([response.actionIdentifier isEqualToString:@"reply_action"]) {
            NSLog(@"%@", response.actionIdentifier);
            completionHandler();
        } else if ([response.actionIdentifier isEqualToString:@"openBucovina_action"]) {
            NSLog(@"%@", response.actionIdentifier);
            completionHandler();
        } else {
            NSLog(@"%@", response.actionIdentifier);
            completionHandler();
        }
    }
}

- (void)loadAttachmentForUrlString:(NSString *)urlString withType:(NSString *)type
                 completionHandler:(void(^)(UNNotificationAttachment *))completionHandler  {
    
    __block UNNotificationAttachment *attachment = nil;
    NSURL *attachmentURL = [NSURL URLWithString:urlString];
    NSString *fileExt = [self fileExtensionForMediaType:type];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session downloadTaskWithURL:attachmentURL
                completionHandler:^(NSURL *temporaryFileLocation, NSURLResponse *response, NSError *error) {
                    if (error != nil) {
                        NSLog(@"%@", error.localizedDescription);
                    } else {
                        NSFileManager *fileManager = [NSFileManager defaultManager];
                        NSURL *localURL = [NSURL fileURLWithPath:[temporaryFileLocation.path stringByAppendingString:fileExt]];
                        [fileManager moveItemAtURL:temporaryFileLocation toURL:localURL error:&error];
                        
                        NSError *attachmentError = nil;
                        attachment = [UNNotificationAttachment attachmentWithIdentifier:@"" URL:localURL options:nil error:&attachmentError];
                        if (attachmentError) {
                            NSLog(@"%@", attachmentError.localizedDescription);
                        }
                    }
                    completionHandler(attachment);
                }] resume];
}
@end
