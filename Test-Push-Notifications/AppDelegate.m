//
//  AppDelegate.m
//  Test-Push-Notifications
//
//  Created by Alex Bofu on 6/20/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "LocationManager.h"

#define SYSTEM_VERSION_GREATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self registerForRemoteNotifications];
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    [self setActions];
    
    return YES;
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.deviceToken = token;
    NSLog(@"Device Token %@", token);
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Remote notification support is unavailable due to error: %@", error);
}

#pragma mark - received notification handler methods

#pragma mark - pre ios 10
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"User Info : %@", userInfo);
}

#pragma mark - after ios 10
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    NSLog(@"User Info from will present : %@",notification.request.content.userInfo);
    NSString *receivedIdentifier = [[notification.request.content.userInfo valueForKey:@"aps"] valueForKey:@"malaca"];
    
    [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
        for (int i = 0; i < notifications.count; i++) {
            if ([[[notifications[i].request.content.userInfo valueForKey:@"aps"] valueForKey:@"malaca"] isEqualToString:receivedIdentifier]) {
                [[UNUserNotificationCenter currentNotificationCenter] removeDeliveredNotificationsWithIdentifiers:@[notifications[i].request.identifier]];
                [[UNUserNotificationCenter currentNotificationCenter] removePendingNotificationRequestsWithIdentifiers:@[notifications[i].request.identifier]];
            }
        }
    }];
    
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    NSLog(@"User Info from did receive : %@",response.notification.request.content.userInfo);
    completionHandler();

    if ([response.notification.request.content.categoryIdentifier isEqualToString:@"bucovina"]) {
        if ([response.actionIdentifier isEqualToString:@"reply_action"]) {
            NSLog(@"%@", response.actionIdentifier);
            completionHandler();
        } else if ([response.actionIdentifier isEqualToString:@"openBucovina_action"]) {
            if ([LocationManager shared].location) {
                NSLog(@"%@", [LocationManager shared].location);
                completionHandler();
            }
            
            NSLog(@"%@", response.actionIdentifier);
        } else {
            NSLog(@"%@", response.actionIdentifier);
            completionHandler();
        }
    }
}

#pragma mark - register for notifications
- (void)registerForRemoteNotifications {
    if(SYSTEM_VERSION_GREATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if (!error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
        }];
    } else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

#pragma mark - notification action buttons
-(void)setActions {
    UNTextInputNotificationAction *replyAction = [UNTextInputNotificationAction
                                                  actionWithIdentifier:@"action1"
                                                  title:@"Rāspunde"
                                                  options:UNNotificationActionOptionNone
                                                  textInputButtonTitle:@"trimite"
                                                  textInputPlaceholder:@"tasteazā rāspunsul"];
    
    UNNotificationAction* openBucovinaAction = [UNNotificationAction
                                                actionWithIdentifier:@"action2"
                                                title:@"Action2"
                                                options:UNNotificationActionOptionForeground];
    
    UNNotificationAction* dismissAction = [UNNotificationAction
                                           actionWithIdentifier:@"action1"
                                           title:@"Cancel"
                                           options:UNNotificationActionOptionDestructive];
    
    UNNotificationCategory* generalCategory = [UNNotificationCategory
                                               categoryWithIdentifier:@"bucovina"
                                               actions:@[dismissAction, replyAction, openBucovinaAction]
                                               intentIdentifiers:@[]
                                               options:UNNotificationCategoryOptionNone];
    
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center setNotificationCategories:[NSSet setWithObjects:generalCategory, nil]];
}
@end
