//
//  AppDelegate.h
//  Test-Push-Notifications
//
//  Created by Alex Bofu on 6/20/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) NSString *deviceToken;

@end

