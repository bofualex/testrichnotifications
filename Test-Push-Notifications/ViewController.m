//
//  ViewController.m
//  Test-Push-Notifications
//
//  Created by Alex Bofu on 6/20/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

#import "ViewController.h"
#import "LocationManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.containerView setHidden:YES];
    [[LocationManager shared] initGeolocationFor:self];
}

-(IBAction)onOk:(id)sender {
    [self.containerView setHidden:YES];
}

-(void)showContainerWithText: (NSString *)text {
    [self.containerView setHidden:NO];
    self.textLabel.text = text;
}

@end
