//
//  LocationManager.h
//  Test-Push-Notifications
//
//  Created by Alex Bofu on 6/21/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

@interface LocationManager : NSObject <CLLocationManagerDelegate>

+ (LocationManager *) shared;

@property (nonatomic) CLLocation * location;
@property (nonatomic) CLLocationManager * locationManager;

-(void)initGeolocationFor: (UIViewController *)sender;

@end
