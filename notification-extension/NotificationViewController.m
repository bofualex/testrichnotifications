//
//  NotificationViewController.m
//  notification-extension
//
//  Created by Alex Bofu on 6/20/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

#import "NotificationViewController.h"
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>

@interface NotificationViewController () <UNNotificationContentExtension>

@property IBOutlet UILabel *label;
@property IBOutlet UILabel *subtitleLabel;
@property IBOutlet UILabel *bodyLabel;
@property IBOutlet UIImageView *notificationImageView;

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setActions];
}

- (void)didReceiveNotification:(UNNotification *)notification {
    NSString *receivedIdentifier = [[notification.request.content.userInfo valueForKey:@"aps"] valueForKey:@"malaca"];
    
    [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
        for (int i = 0; i < notifications.count; i++) {
            if ([[[notifications[i].request.content.userInfo valueForKey:@"aps"] valueForKey:@"malaca"] isEqualToString:receivedIdentifier]) {
                [[UNUserNotificationCenter currentNotificationCenter] removeDeliveredNotificationsWithIdentifiers:@[notifications[i].request.identifier]];
                [[UNUserNotificationCenter currentNotificationCenter] removePendingNotificationRequestsWithIdentifiers:@[notifications[i].request.identifier]];
            }
        }
    }];
    
    self.label.text = notification.request.content.title;
    self.subtitleLabel.text = notification.request.content.subtitle;
    self.bodyLabel.text = notification.request.content.body;
    
    NSDictionary *dict = notification.request.content.userInfo;
    
    if (dict[@"data"] && [dict[@"data"] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *notificationData = dict[@"data"];
        
        if (notificationData[@"attachment-url"] && [notificationData[@"attachment-url"] isKindOfClass:[NSString class]]) {
            NSString *urlString = notificationData[@"attachment-url"];
            NSURL *fileUrl = [NSURL URLWithString:urlString];
            
            NSData *data = [NSData dataWithContentsOfURL:fileUrl];
            UIImage *image = [UIImage imageWithData:data];
            
            [self.notificationImageView setImage:image];
        }
    }
}

-(void)setActions {
    UNTextInputNotificationAction *replyAction = [UNTextInputNotificationAction
                                                  actionWithIdentifier:@"action1"
                                                  title:@"Rāspunde"
                                                  options:UNNotificationActionOptionNone
                                                  textInputButtonTitle:@"trimite"
                                                  textInputPlaceholder:@"tasteazā rāspunsul"];
    
    UNNotificationAction* openBucovinaAction = [UNNotificationAction
                                                actionWithIdentifier:@"action2"
                                                title:@"Una în plus"
                                                options:UNNotificationActionOptionForeground];
    
    UNNotificationAction* dismissAction = [UNNotificationAction
                                           actionWithIdentifier:@"action1"
                                           title:@"Anuleazā"
                                           options:UNNotificationActionOptionDestructive];
    
    UNNotificationCategory* generalCategory = [UNNotificationCategory
                                               categoryWithIdentifier:@"bucovina"
                                               actions:@[dismissAction, replyAction, openBucovinaAction]
                                               intentIdentifiers:@[]
                                               options:UNNotificationCategoryOptionNone];
    
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center setNotificationCategories:[NSSet setWithObjects:generalCategory, nil]];
}
@end
